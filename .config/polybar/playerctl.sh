#!/bin/bash

# Try if a player is active and redirect stderr in case an error comes up
# (if no player is found)
playerstatus=$(playerctl status 2>/dev/null)

if [ $? -eq 0 ]; then

	# Retrieve title and artist
	title=$(playerctl metadata title)
	artist=$(playerctl metadata artist)

	echo "$title - $artist"

else
	echo ""
fi
