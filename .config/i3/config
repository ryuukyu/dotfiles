# i3 config file (v4)
#
# Please see https://i3wm.org/docs/userguide.html for a complete reference!

#
# General
#

# Disable automatic focus switching and mouse
focus_follows_mouse no
# Disable automatic mouse repositioning to new focus
mouse_warping none

# Set Mod Key to the Windows Button
set $mod Mod4

# reload the configuration file
bindsym $mod+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart
# exit i3 (logs you out of your X session)
bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'i3-msg exit'"



#
# Startup
#

# Start polybar
exec --no-startup-id ~/.config/polybar/launch.sh

# Start compton
exec --no-startup-id compton --config ~/.config/compton/config

# Set Wallpaper
exec --no-startup-id feh --bg-fill ~/.config/wallpapers/forest_warm.png



#
# Appearance
#

# Font for window titles. Will also be used by the bar unless a different font
# is used in the bar {} block below.
font pango:monospace 8

# Turn of window titles and and set border width
for_window [class="^.*"] border pixel 3

# Set Gap size
gaps outer 0
gaps inner 10



#
# Workspaces
#

# Define names for default workspaces for which we configure key bindings later on.
# We use variables to avoid repeating the names in multiple places.
set $ws1 "1"
set $ws2 "2"
set $ws3 "3"
set $ws4 "4"
set $ws5 "5"
set $ws6 "6"
set $ws7 "7"
set $ws8 "8"
set $ws9 "9"
set $ws10 "10"

# Define monitors for workspaces
workspace $ws1  output DVI-I-1
workspace $ws3  output DVI-I-1
workspace $ws5  output DVI-I-1
workspace $ws7  output DVI-I-1
workspace $ws9  output DVI-I-1
workspace $ws2  output HDMI-0
workspace $ws4  output HDMI-0
workspace $ws6  output HDMI-0
workspace $ws8  output HDMI-0
workspace $ws10 output HDMI-0

# Keybinds for workspace switching
bindsym $mod+1 workspace $ws1
bindsym $mod+2 workspace $ws2
bindsym $mod+3 workspace $ws3
bindsym $mod+4 workspace $ws4
bindsym $mod+5 workspace $ws5
bindsym $mod+6 workspace $ws6
bindsym $mod+7 workspace $ws7
bindsym $mod+8 workspace $ws8
bindsym $mod+9 workspace $ws9
bindsym $mod+0 workspace $ws10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace $ws1
bindsym $mod+Shift+2 move container to workspace $ws2
bindsym $mod+Shift+3 move container to workspace $ws3
bindsym $mod+Shift+4 move container to workspace $ws4
bindsym $mod+Shift+5 move container to workspace $ws5
bindsym $mod+Shift+6 move container to workspace $ws6
bindsym $mod+Shift+7 move container to workspace $ws7
bindsym $mod+Shift+8 move container to workspace $ws8
bindsym $mod+Shift+9 move container to workspace $ws9
bindsym $mod+Shift+0 move container to workspace $ws10



#
# Window Keybinds
#

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# Kill focused window
bindsym $mod+Shift+q kill

# change focus
bindsym $mod+Left  focus left
bindsym $mod+Down  focus down
bindsym $mod+Up    focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+Left  move left
bindsym $mod+Shift+Down  move down
bindsym $mod+Shift+Up    move up
bindsym $mod+Shift+Right move right

# split in horizontal orientation
bindsym $mod+h split h

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent

# focus the child container
#bindsym $mod+d focus child

# resize window (you can also use the mouse for that)
mode "resize" {
    # These bindings trigger as soon as you enter the resize mode

    # Pressing left will shrink the window’s width.
    # Pressing right will grow the window’s width.
    # Pressing up will shrink the window’s height.
    # Pressing down will grow the window’s height.

    # same bindings, but for the arrow keys
    bindsym Left resize shrink width 10 px or 10 ppt
    bindsym Down resize grow height 10 px or 10 ppt
    bindsym Up resize shrink height 10 px or 10 ppt
    bindsym Right resize grow width 10 px or 10 ppt

    # back to normal: Enter or Escape or $mod+r
    bindsym Return mode "default"
    bindsym Escape mode "default"
    bindsym $mod+r mode "default"
}

bindsym $mod+r mode "resize"



#
# Applications
#

# Start a terminal
bindsym $mod+Return exec i3-sensible-terminal

# Start rofi
bindsym $mod+d exec rofi -show combi

# Application Shortcuts
mode "Launch App" {
    # Launch an App and go back to normal mode
    bindsym c exec google-chrome-stable;    mode "default"
    bindsym d exec nautilus;                mode "default"
    bindsym s exec spotify;                 mode "default"

    # back to normal: Enter or Escape or $mod+c
    bindsym Return mode "default"
    bindsym Escape mode "default"
    bindsym $mod+c mode "default"
}

bindsym $mod+c mode "Launch App"



#
# Other Keybinds
#

# Volume Keys
bindsym XF86AudioRaiseVolume exec "pactl set-sink-volume @DEFAULT_SINK@ +1000"
bindsym XF86AudioLowerVolume exec "pactl set-sink-volume @DEFAULT_SINK@ -1000"
bindsym XF86AudioMute exec "pactl set-sink-mute @DEFAULT_SINK@ toggle"

# Media Keys
bindsym XF86AudioPlay exec "playerctl play-pause"
bindsym XF86AudioPrev exec "playerctl previous"
bindsym XF86AudioNext exec "playerctl next"
